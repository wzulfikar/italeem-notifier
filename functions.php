<?php
function curl($url,$u,$p){
$ch = curl_init();    
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

curl_setopt($ch, CURLOPT_URL, $url); 
$cookie = 'cookies.txt';
$timeout = 30;

curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_TIMEOUT,         10); 
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,  $timeout );
curl_setopt($ch, CURLOPT_COOKIEJAR,       $cookie);
curl_setopt($ch, CURLOPT_COOKIEFILE,      $cookie);

curl_setopt ($ch, CURLOPT_POST, 1); 
curl_setopt ($ch,CURLOPT_POSTFIELDS,"username=$u&password=$p");     

$result = curl_exec($ch);

/* //OPTIONAL - Redirect to another page after login
$url = "http://aftabcurrency.com/some_other_page";
curl_setopt ($ch, CURLOPT_POST, 0); 
curl_setopt($ch, CURLOPT_URL, $url);
$result = curl_exec($ch);
 */ //end OPTIONAL 

curl_close($ch); 
return $result;
}
function scrape_between($data, $start, $end){
     $data = stristr($data, $start); // Stripping all data from before $start
     $data = substr($data, strlen($start));  // Stripping $start
     $stop = stripos($data, $end);   // Getting the position of the $end of the data to scrape
     $data = substr($data, 0, $stop);    // Stripping all data from after and including the $end of the data to scrape
     return $data;   // Returning the scraped data from the function
}
function initPage($u,$p){
//Credit to http://stackoverflow.com/users/570759/jsherk
//From his answer on http://stackoverflow.com/questions/11171713/scrape-a-site-content-with-a-secure-login
 $scraped_page = curl("http://italeem.iium.edu.my/2014/login/index.php",$u,$p);    // Downloading IMDB home page to variable $scraped_page
 $startTag = '<div class="header">';
 $endTag = '<span id=';

/*
$html = new simple_html_dom();
$html->load($scraped_page);
$ret = $html->find('div[id^=course-]');

foreach($ret as $key => $val){
	echo $val;
}
*/
$scraped_data = scrape_between($scraped_page, $startTag, $endTag);   // Scraping downloaded dara in $scraped_page for content between <title> and </title> tags
return $scraped_data;
}
?>