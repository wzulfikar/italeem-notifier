var courses = document.querySelectorAll('.title a'),
    courseCount = courses.length,
    courseDetailPage = [],
    courseDetailId,
    courseTitle = [],
    courseLink = [],
    courseCode = [],
    courseMoodleId = [],
    courseSection = [],
    courseInstructor = [],
    courseAssignment = [],
    courseAssignmentCount = [],
    courseForum = [],
    courseForumCount = [],
    assignmentTitle,
    assignmentDue,
    assignmentSubmission,
    titleTemp = "",
    sectionTemp = "",
    assignTemp,
    s = "",
    NOASSIGNMENTSTRING = "No Assignment at the moment",
    courseRows = document.getElementById('course-rows'),
    temp = [],
    openTag = "",
    closeTagNoLi = '</tr></tbody></table></a>',
    courseDetail,
    assignmentCountTemp = [],
    testAssignment = [],
    assignmentDetailRows=""
    ;
  
for (var i=0; i < courseCount; i++){
   
   titleTemp = courses[i].innerHTML.split(' ',3);
   sectionTemp = courses[i].innerHTML.replace(titleTemp.join(' ')+" ","");
   sectionTemp = sectionTemp.split(' ',2);
   
   courseSection[i] = sectionTemp.join(' ');
   
   courseTitle.push(courses[i].innerHTML.replace(titleTemp.join(' ')+" "+sectionTemp.join(' ')+" ",""));
   titleTemp.splice(0,1);

   courseDetailId = titleTemp.join("-");
   
   openTag = '<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child"></li><li>';
   openTag += '<a href="#'+courseDetailId+'" data-transition="slide">';
    
   courseLink.push(courses[i].href);
   courseMoodleId.push(courseLink[i].split("id=")[1]);
   console.log(courseMoodleId[i]);
   courseCode.push(titleTemp.join('<br/>'));
   courseCode[i] = '<div class="course-code-box">'+courseCode[i]+'</div>';
   courseDetail = 
   '<div class="course-info"><h2 class="ui-li-heading">'+courseTitle[i]+'</h2><p class="ui-li-desc">'+courseSection[i]+' : '+courseInstructor[i]+'</p></div>';
   
   temp.push(document.querySelectorAll("#region_"+courseMoodleId[i]+"_assign_inner"));
   courseAssignment.push(temp[i].item(0)||"No Assignment");
   
   assignmentCountTemp.push(document.getElementById("region_"+courseMoodleId[i]+"_assign_inner")||"No Assignment");
   courseAssignmentCount.push(assignmentCountTemp[i].childElementCount||0);
   courseRows.innerHTML += openTag+courseCode[i]+courseDetail+'<span class="ui-li-count">'+courseAssignmentCount[i]+'</span></a></li>';
   
   //Cook all info for course detail page
   assignmentDetailRows = "";
   var atest = document.getElementById("region_"+courseMoodleId[i]+"_assign_inner");
   for(var t = 0;t<courseAssignmentCount[i];t++){
   var x = atest.childNodes[t]||"0";
   var assignmentName = (x.querySelector("a[title='Assignment']").innerHTML.replace("Assignment: ",""));//assignment name
   console.log(assignmentName);
   var date = (x.querySelector(".info").innerHTML.replace("Due date: ",""));//Due date
   var submission = (x.querySelector(".details").innerHTML.replace("My submission: ",""));//Submission

   //get due time with format hh:mmAM / hh:mmPM
   var timeTemp = date.split(",");
   timeTemp.splice(0,2);//remove space & day
   var dueTime = timeTemp[0].split(" ").join("");
   //console.log("time: "+time);
   
   //get due date with format 31DEC
   var dateTemp = date.split(" ",3);
   dateTemp.splice(0,1);
   var month = dateTemp[1].substr(0,3).toUpperCase();
   var dueDate = (dateTemp[0]+" "+month);
   
   assignmentDetailRows += '<li>';
   assignmentDetailRows += '<a>';
   assignmentDetailRows += '<div class="course-detail-box">'+dueDate+'<br/>'+dueTime+'</div>';
   assignmentDetailRows += '<div class="course-info"><h2 class="ui-li-heading">'+assignmentName+'</h2><p class="ui-li-desc">'+submission+'</p></div>';
   //assignmentDetailRows += '<div class="course-info">';
   //assignmentDetailRows += '<h2 class="ui-li-heading" id="assignmentName">'+assignmentName+'</h2>';
   //assignmentDetailRows += '<p class="ui-li-desc">'+courseSection[i]+' : '+courseInstructor[i]+'</p></div>';
   assignmentDetailRows += '</a>';
   assignmentDetailRows += '</li>';
   }
   
   //Build the courseDetailPage
   s = '<div data-role="page" id="'+courseDetailId+'">';
 	s += '<div data-role="header"><h1>'+courseDetailId.replace("-"," ")+'</h1></div>';
 	s += '<div data-role="main" class="ui-content">';
 	s += '<ul id="course-detail-rows" data-role="listview" data-filter="true" data-input="courseDetailFilter" data-inset="true">';
 	s += '<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child">'+courseAssignmentCount[i]+' Assignments</li>';
 	s += assignmentDetailRows;
 	s += '</ul>';
 	s += '</div>';
 	s += '<div data-role="footer" data-fullscreen="true" data-position="fixed">';
 	s += '<h1><a href="#landing-page" data-transition="slide" data-direction="reverse">Back to dasboard</a></h1>';
 	s += '</div>';
 	s += '</div>';
   courseDetailPage.push(s);
 }
 	
for (var i=0; i < courseCount; i++){
	document.write(courseDetailPage[i]);
}